package exceptions;

public class BadgeException extends Exception{
    public BadgeException(String message) {
        super(message);
    }

}
