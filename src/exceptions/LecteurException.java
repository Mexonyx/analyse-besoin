package exceptions;

public class LecteurException extends Exception{
    public LecteurException(String message) {
        super(message);
    }

}
