package exceptions;

import interfaces.PorteInterface;

public class OuvertureException extends Exception {
    private final PorteInterface porte;

    public OuvertureException(String message, PorteInterface porte) {
        super(message);
        this.porte = porte;
    }

    public PorteInterface getPorte() {
        return porte;
    }
}