package interfaces;

import exceptions.OuvertureException;

public interface MoteurInterface {
    void InterrogerLecteurs(LecteurInterface... lecteurs) throws OuvertureException;
    boolean isReaderConnected(LecteurInterface lecteur);

}