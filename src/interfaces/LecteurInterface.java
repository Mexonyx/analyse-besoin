package interfaces;

public interface LecteurInterface {
    boolean badgeDetecter();

    PorteInterface[] getPortes();
}
