package interfaces;

public interface PorteInterface {
    void open();
    boolean estOuverte();
    void bipSiFerme();
}