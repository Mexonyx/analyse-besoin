package utilities;

import interfaces.PorteInterface;

public class PorteSpy implements PorteInterface {
    private boolean ouvertureDemandee = false;
    private boolean doorBlocked = false;

    public boolean isDoorBlocked() {
        return doorBlocked;
    }

    @Override
    public void open() {
        ouvertureDemandee = true;
    }

    @Override
    public boolean estOuverte() {
        return ouvertureDemandee;
    }

    @Override
    public void bipSiFerme() {
        if (doorBlocked) {
            System.out.println("Bip - Porte bloquée.");
            return;
        }

        if (!ouvertureDemandee) {
            System.out.println("Bip - La porte est fermée.");
        } else {
            System.out.println("La porte est ouverte.");
        }
    }

    public void close() {
        ouvertureDemandee = false;
    }

    public void bloquerPorte() {
        doorBlocked = true;
    }
}
