package utilities;

import exceptions.BadgeException;
import interfaces.PorteInterface;
import interfaces.LecteurInterface;
import Enum.Authorizations;

import java.time.LocalDate;

public class LecteurFake implements LecteurInterface {
    private final PorteInterface[] portes;
    private Authorizations authorizations;
    private boolean badgeDetected;
    private int consecutiveBadgePresentations;
    private final int MAX_CONSECUTIVE_PRESENTATIONS = 3;
    public LecteurFake(Authorizations authorizations, PorteInterface... portes) {
        this.portes = portes;
        this.authorizations = authorizations;
        this.badgeDetected = false;
        this.consecutiveBadgePresentations = 0;
    }

    public Authorizations getAuthorizations() {
        return authorizations;
    }

    public void presentationDeBadge(FakeBadge badge) throws BadgeException {
        if (badge == null) {
            throw new BadgeException("Badge non détecté ou problème avec le badge");
        }

        if (!badge.getLocked() && !badge.getDeficient() &&
                badge.getDateExpiration().isAfter(LocalDate.now()) &&
                authorizations.equals(badge.getRole())) {
            badgeDetected = true;
            consecutiveBadgePresentations++;
            if (consecutiveBadgePresentations > MAX_CONSECUTIVE_PRESENTATIONS) {
                for (PorteInterface porte : portes) {
                    if (porte instanceof PorteSpy) {
                        ((PorteSpy) porte).bloquerPorte();
                    }
                }
            }
        } else {
            badgeDetected = false;
            throw new BadgeException("La lecture du badge a rencontré un problème.");
        }
    }

    @Override
    public boolean badgeDetecter() {
        return badgeDetected;
    }

    @Override
    public PorteInterface[] getPortes() {
        return portes;
    }
}
