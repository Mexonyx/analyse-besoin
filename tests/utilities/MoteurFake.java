package utilities;

import exceptions.OuvertureException;
import interfaces.LecteurInterface;
import interfaces.MoteurInterface;

public class MoteurFake implements MoteurInterface {
    private LecteurInterface connectedReader;

    @Override
    public void InterrogerLecteurs(LecteurInterface... lecteurs) throws OuvertureException {
        for (var lecteur : lecteurs) {
            if (lecteur.badgeDetecter()) {
                for (var porte : lecteur.getPortes()) {
                    if (!porte.estOuverte()) {
                        porte.open();
                        if (!porte.estOuverte()) {
                            throw new OuvertureException("Erreur lors de l'ouverture de la porte.", porte);
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean isReaderConnected(LecteurInterface lecteur) {
        return lecteur == connectedReader;
    }

    public void connectReader(LecteurInterface lecteur) {
        connectedReader = lecteur;
    }
}
