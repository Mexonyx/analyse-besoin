package utilities;

import java.time.LocalDate;
import Enum.Authorizations;

public class FakeBadge {
    private String id;
    private Authorizations authorizations;
    private LocalDate dateExpiration;
    private boolean isLocked;
    private boolean isDeficient;

    public FakeBadge(String id, Authorizations authorizations, LocalDate dateExpiration){
        this.id = id;
        this.authorizations = authorizations;
        this.dateExpiration = dateExpiration;
        this.isLocked = false;
        this.isDeficient = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Authorizations getRole() {
        return authorizations;
    }

    public void setRole(Authorizations authorizations) {
        this.authorizations = authorizations;
    }

    public LocalDate getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(LocalDate dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public boolean getLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public boolean getDeficient() {
        return isDeficient;
    }

    public void setDeficient(boolean deficient) {
        isDeficient = deficient;
    }
}
