package utilities;

import java.time.LocalDate;
import Enum.Authorizations;
import exceptions.BadgeException;
import exceptions.OuvertureException;
import interfaces.MoteurInterface;
import org.junit.Test;


import static org.junit.Assert.*;

public class ControleAccesTest {

    private static LocalDate dateExpirationOK = LocalDate.of(2025, 1, 8);
    private static LocalDate dateExpirationKO = LocalDate.of(2022, 1, 8);

    @Test
    public void TestOk(){
        assertTrue(true);
    }

    @Test
    public void OuverturePorte() {
        var porteSpy = new PorteSpy();
        var moteurFake = new MoteurFake();
        var lecteurFake = new LecteurFake(Authorizations.EMPLOYEES, porteSpy);

        try {
            lecteurFake.presentationDeBadge(new FakeBadge("54878541268", Authorizations.EMPLOYEES, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake);
            assertTrue(porteSpy.estOuverte());
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void pasInterrogation() {
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.EMPLOYEES, porteSpy);

        assertFalse(lecteurFake.badgeDetecter());
        assertFalse(porteSpy.estOuverte());
    }

    @Test
    public void badgeNonDetecter(){
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.AGENT, porteSpy);

        assertFalse(lecteurFake.badgeDetecter());
        assertFalse(porteSpy.estOuverte());
    }

    @Test
    public void lecteurPlusieursPortes(){
        var porteSpy1 = new PorteSpy();
        var porteSpy2 = new PorteSpy();
        var moteurFake = new MoteurFake();

        var lecteurFake = new LecteurFake(Authorizations.AGENT, porteSpy1, porteSpy2);
        try {
            lecteurFake.presentationDeBadge(new FakeBadge("8451228623", Authorizations.AGENT, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake);
            assertTrue(porteSpy1.estOuverte());
            assertTrue(porteSpy2.estOuverte());
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void plusieursLecteurs() {
        var porteSpy = new PorteSpy();
        var moteurFake = new MoteurFake();
        var lecteurFake1 = new LecteurFake(Authorizations.AGENT, porteSpy);
        var lecteurFake2 = new LecteurFake(Authorizations.AGENT, porteSpy);

        try {
            lecteurFake2.presentationDeBadge(new FakeBadge("7895532259523", Authorizations.AGENT, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake1, lecteurFake2);
            assertTrue(porteSpy.estOuverte());
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void multiplePorteMultipleLecteurs() {
        var porteSpy1 = new PorteSpy();
        var porteSpy2 = new PorteSpy();
        var lecteurFake1 = new LecteurFake(Authorizations.AGENT, porteSpy1);
        var lecteurFake2 = new LecteurFake(Authorizations.AGENT, porteSpy2);
        var moteurFake = new MoteurFake();

        try {
            lecteurFake2.presentationDeBadge(new FakeBadge("126866325", Authorizations.AGENT, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake1, lecteurFake2);
            assertFalse(porteSpy1.estOuverte());
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void OuverturePorteBadgeException() {
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.AGENT, porteSpy);

        assertThrows(BadgeException.class, () -> {
            lecteurFake.presentationDeBadge(null);
        });
    }

    @Test
    public void BadgeNonAutorise() {
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.EMPLOYEES, porteSpy);
        var moteurFake = new MoteurFake();

        try {
            lecteurFake.presentationDeBadge(new FakeBadge("54878541268", Authorizations.AGENT, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake);
            assertFalse(porteSpy.estOuverte());
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void BadgeNonAutoriseMultiplePorte() {
        var porteSpy1 = new PorteSpy();
        var porteSpy2 = new PorteSpy();
        var lecteurFake1 = new LecteurFake(Authorizations.CLEANERS, porteSpy1, porteSpy2);
        assertThrows(BadgeException.class, () -> {
            lecteurFake1.presentationDeBadge(new FakeBadge("126866325", Authorizations.AGENT, dateExpirationOK));
        });
        assertFalse(porteSpy1.estOuverte());
        assertFalse(porteSpy2.estOuverte());
    }

    @Test
    public void BadgeExpire() {
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.EMPLOYEES, porteSpy);

        assertThrows(BadgeException.class, () -> {
            lecteurFake.presentationDeBadge(new FakeBadge("54878541268", Authorizations.EMPLOYEES, dateExpirationKO));
        });
        assertFalse(porteSpy.estOuverte());
    }

    @Test
    public void BadgeNonExpire() {
        var moteurFake = new MoteurFake();
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.EMPLOYEES, porteSpy);

        try {
            lecteurFake.presentationDeBadge(new FakeBadge("54878541268", Authorizations.EMPLOYEES, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake);
            assertTrue(porteSpy.estOuverte());
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void BipQuandPorteFermee() {
        var moteurFake = new MoteurFake();
        var porteSpy = new PorteSpy();

        LecteurFake lecteurFake = new LecteurFake(Authorizations.AGENT, porteSpy);
        try {
            lecteurFake.presentationDeBadge(new FakeBadge("54878541268", Authorizations.AGENT, dateExpirationOK));
            moteurFake.InterrogerLecteurs(lecteurFake);
            assertTrue(porteSpy.estOuverte());

            porteSpy.close();

            assertFalse(porteSpy.estOuverte());
            porteSpy.bipSiFerme();
        } catch (BadgeException | OuvertureException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void connexionMoteurLecteur() {
        var moteurFake = new MoteurFake();
        var porteSpy = new PorteSpy();

        LecteurFake lecteurFake = new LecteurFake(Authorizations.AGENT, porteSpy);
        moteurFake.connectReader(lecteurFake);

        assertTrue(moteurFake.isReaderConnected(lecteurFake));
    }

    @Test
    public void badgePresentation_ExceedMaxConsecutiveTimes_BeepAndBlockDoor() {
        var porteSpy = new PorteSpy();
        var lecteurFake = new LecteurFake(Authorizations.EMPLOYEES, porteSpy);

        try {
            for (int i = 0; i < 4; i++) {
                lecteurFake.presentationDeBadge(new FakeBadge("54878541268", Authorizations.EMPLOYEES, dateExpirationOK));
            }

            assertFalse(porteSpy.estOuverte());
            assertTrue(porteSpy.isDoorBlocked());
        } catch (BadgeException e) {
            e.printStackTrace();
        }
    }
}
